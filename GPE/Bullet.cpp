#include "StdAfx.h"
#include "resource.h"
#include "AnimatedSprite.h"
#include "Bullet.h"


const float CONST_MOVE_SPEED = 650.0f;

CBullet::CBullet(void)
{
	//todo: extend projectile class
	//level class to preload all projectiles
	//rename this damn class or move the sprite to a missle class
	//create missle class to extend projectile
	m_fX = 0;
	m_fY = 0;

	m_bFinished = false;
}


CBullet::~CBullet(void)
{
	Deinitialise();
}


bool CBullet::Deinitialise()
{
	return (true);
}

bool CBullet::Initialise(float initXpos, float initYpos)
{
	int iMask = IDB_BULLETMASK;
	int iSprite = IDB_BULLET;

	m_fX = initXpos;
	m_fY = initYpos;

	m_pAnim = new CAnimatedSprite();
	m_pAnim->Initialise(iSprite, iMask);
	m_pAnim->SetWidth(20);
	m_pAnim->SetSpeed(0.1f);
	m_pAnim->AddFrame(0);
	m_pAnim->AddFrame(20);

	return (true);
}

void CBullet::Render()
{
	m_pAnim->Render();
}

void CBullet::Process(float a_fDeltaTick)
{
	m_pAnim->Process(a_fDeltaTick);

	m_fY -= CONST_MOVE_SPEED * a_fDeltaTick;

	m_pAnim->SetX(static_cast<int>(m_fX));
	m_pAnim->SetY(static_cast<int>(m_fY));

	if (m_fY < 10) //if bullet goes offscreen delete it (not quite offscreen in this case)
	{
		m_bFinished = true;
	}
}

bool CBullet::GetFinished()
{
	return m_bFinished;
}

void CBullet::SetFinished(bool status)
{
	m_bFinished = status;
}

void CBullet::ExplodeBullet()
{
	//do explode animation?
	//handle here? or after collision in a differnt sprite class

	//TODO: set it as a state, then on collision set the state to explode. play animation then delete it
}


CBoundingRect CBullet::GetBoundingRect()
{
	m_BoundingRect.x1 = m_fX - 10;
	m_BoundingRect.x2 = m_fX + 10; 

	m_BoundingRect.y1 = m_fY - 10;
	m_BoundingRect.y2 = m_fY + 10;

	return m_BoundingRect;
}


float CBullet::GetX()
{
	return m_fX;
}


float CBullet::GetY()
{
	return m_fY;
}