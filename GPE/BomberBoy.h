#pragma once

#include "BoundingRect.h"

class CAnimatedSprite;

class CBomberBoy
{
	public:
		enum EFacing
		{
			INVALID_FACING = -1,
			FACING_FORWARD,
			FACING_LEFT,
			FACING_RIGHT,
			FACING_BACKWARD,
			MAX_FACING
		};

		enum EBomberBoyColour
		{
			INVALID_BOMBERBOYCOLOUR,
			BOMBERBOYCOLOUR_WHITE,
			BOMBERBOYCOLOUR_BLACK,
			MAX_BOMBERBOYCOLOUR
		};

	public:
		CBomberBoy(float a_fX, float a_fY);
		virtual ~CBomberBoy();

		virtual bool Deinitialise();
		virtual bool Initialise(EBomberBoyColour a_eColour);

		virtual void Render();
		virtual void Process(float a_fDeltaTick);
		void RandomiseLocation();

		void SetStopWalkingState();
		void SetWalkState(EFacing a_eDirection);
		void SetStopWalkingState(EFacing a_eDirection);

		CBoundingRect GetBoundingRectForNextFrame(float a_fDeltaTick); 
		void PushBomberBoyBack();

		float GetX();
		float GetY();

	private:
		void Walk();
		bool				m_bWalking[MAX_FACING];
		CAnimatedSprite		*m_pAnim[MAX_FACING];
		float				m_fX;
		float				m_fY;
		EFacing				m_eFacing;
		EFacing				m_eDirection; 
		CBoundingRect		m_BoundingRect; 
		bool				m_PushBack;

};


