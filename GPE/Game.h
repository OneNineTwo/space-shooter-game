#pragma once
#include <windows.h>

class CBackBuffer;
class CLevel; 
class CController; 
class CClock; 
class CSoundFX; 

class CGame
{
	public:
		virtual ~CGame();

		virtual bool Initialise(HINSTANCE a_hInstance, HWND a_hWnd, int a_iWidth, int a_iHeight);

		static CGame& GetInstance();
		static void DestroyInstance();
		
		void ExecuteOneFrame();
		virtual void Draw();
		virtual void Process(float a_fDeltaTick);

		CLevel* GetLevel();
		CSoundFX* GetSoundFXPlayer(); 
		void ProcessKeyDownMessage(char a_wParam);
		void ProcessKeyUpMessage(char a_wParam);
		
		
		CBackBuffer* GetBackBuffer();
		HINSTANCE GetAppInstance();
		HWND GetWindow();
		HDC GetSpriteDC();



	private:
		CGame();
		CGame(const CGame& a_rGame);
		CGame& operator= (const CGame& a_rGame);

		CClock*			m_pClock;
		CBackBuffer*	m_pBackBuffer;
		CLevel*			m_pLevel; 
		CSoundFX*		m_pSoundFX;
		CController*	m_pController; 

		HINSTANCE m_hApplicationInstance;
		HWND m_hMainWindow;
		HDC m_hSpriteDC;

		static CGame* s_pGame;

};