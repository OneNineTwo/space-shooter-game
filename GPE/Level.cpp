#include "stdafx.h"
#include "Level.h"
#include "Game.h"
#include "BomberBoy.h"//why cant i delete this
#include "SoundFX.h"
#include "BoundingRect.h"
#include "Terrain.h"
#include "PlayerHealth.h"
#include "Bullet.h"
#include "PlayerShip.h"
#include "AlienShip.h"
#include "AlienProjectile1.h"
#include "PowerUp.h"
#include "SpeedPowerUp.h"
#include "HealthPowerUp.h"
#include "resource.h"
#include "Sprite.h"

#include <time.h>
#include <stdlib.h>

CLevel::CLevel()
{
	srand(time(NULL));
}

CLevel::~CLevel()
{
	delete m_pTerrain;
	m_pTerrain = NULL;

	delete m_pPlayerHealth;
	m_pPlayerHealth = NULL;

	delete m_pPlayer;
	m_pPlayer = NULL;

	delete m_gameover;
	m_gameover = NULL;

	if (m_PlayerProjectiles.size() > 0)
	{
		for (int i=0; i < m_PlayerProjectiles.size(); i++)
		{
			delete m_PlayerProjectiles.at(i);
			m_PlayerProjectiles.erase(m_PlayerProjectiles.begin() + i);
		}
	}

	if (m_AlienProjectiles.size() > 0)
	{
		for (int i=0; i < m_AlienProjectiles.size(); i++)
		{
			delete m_AlienProjectiles.at(i);
			m_AlienProjectiles.erase(m_AlienProjectiles.begin() + i);
		}
	}

	if (m_PowerUps.size() > 0)
	{
		for (int i=0; i < m_PowerUps.size(); i++)
		{
			delete m_PowerUps.at(i);
			m_PowerUps.erase(m_PowerUps.begin() + i);
		}
	}

	Deinitialise();
}

bool CLevel::Deinitialise()
{
	return (true);
}

bool CLevel::Initialise(int a_iWidth, int a_iHeight)
{
	m_iTimePassed = 0;
	m_iTimeLastPowerUp = 0;
	m_bPlayerShooting = false;

	m_WavesFinished.push_back(false);//wave 0 or enum WAVE_1
	m_WavesFinished.push_back(false);//wave 1 or enum WAVE_2
	m_WavesFinished.push_back(false);//wave 2 or enum WAVE_3
	m_WavesFinished.push_back(false);//wave 3 or enum WAVE_4

	m_iWidth = a_iWidth;
	m_iHeight = a_iHeight;

	m_pTerrain = new CTerrain();
	m_pTerrain->Initialise();

	m_pPlayerHealth = new CPlayerHealth();
	m_pPlayerHealth->Initialise();

	m_pPlayer = new CPlayerShip();
	m_pPlayer->Initialise();

	//quick gameover hacked together
	int iMask = IDB_GAMEOVERMASK;
	int iSprite = IDB_GAMEOVERSPRITE;
	m_gameover = new CSprite();
	m_gameover->Initialise(iSprite, iMask);
	m_gameover->SetX(400);
	m_gameover->SetY(400);
	

	return (true);
}

void CLevel::Render()
{
	m_pTerrain->Render();
	m_pPlayerHealth->Render();

	if(m_PowerUps.size() > 0)
	{
		for (int i=0; i < m_PowerUps.size(); i++)
		{
			 m_PowerUps.at(i)->Render();
		}
	}

	if(m_PlayerProjectiles.size() > 0)
	{
		for (int i=0; i < m_PlayerProjectiles.size(); i++)
		{
			 m_PlayerProjectiles.at(i)->Render();
		}
	}
	
	if(m_AlienProjectiles.size() > 0)
	{
		for (int i=0; i < m_AlienProjectiles.size(); i++)
		{
			 m_AlienProjectiles.at(i)->Render();
		}
	}
	
	if(m_AlienShips.size() > 0)
	{
		for (int i=0; i < m_AlienShips.size(); i++)
		{
			 m_AlienShips.at(i)->Render();
		}
	}

	

	if(m_pPlayerHealth->GetCurrentHealth() <= 0)
	{
		m_gameover->Render();
	}


	m_pPlayer->Render();
}

void CLevel::Process(float a_fDeltaTick)
{
	m_iTimePassed += a_fDeltaTick*1000;


	ProcessTimeEvents(a_fDeltaTick);


	int playerShootTimer = m_pPlayer->GetShootTimer();
	playerShootTimer += a_fDeltaTick*1000;
	m_pPlayer->SetShootTimer(playerShootTimer);
	if (m_bPlayerShooting && m_pPlayer->GetShootTimer() > 500)
	{
		CreateProjectile();
		m_pPlayer->SetShootTimer(0);
	}


	ProcessCollisions(a_fDeltaTick);


	m_pTerrain->Process(a_fDeltaTick);

	m_pPlayerHealth->Process(a_fDeltaTick);

	m_pPlayer->Process(a_fDeltaTick);


	if (m_PlayerProjectiles.size() > 0)
	{
		for (int i=0; i < m_PlayerProjectiles.size(); i++)
		{
			m_PlayerProjectiles.at(i)->Process(a_fDeltaTick);
			if (m_PlayerProjectiles.at(i)->GetFinished())
			{
				delete m_PlayerProjectiles.at(i);
				m_PlayerProjectiles.erase(m_PlayerProjectiles.begin() + i);
			}
			/*
			for (std::deque<CBullet*>::const_iterator it = m_PlayerProjectiles.begin(); it != m_PlayerProjectiles.end(); ++it)
			{
				delete *it;
			}
			*/
		}
	}

	
	if (m_AlienProjectiles.size() > 0)
	{
		for (int i=0; i < m_AlienProjectiles.size(); i++)
		{
			m_AlienProjectiles.at(i)->Process(a_fDeltaTick);
			if (m_AlienProjectiles.at(i)->GetFinished())
			{
				delete m_AlienProjectiles.at(i);
				m_AlienProjectiles.erase(m_AlienProjectiles.begin() + i);
			}
		}
	}


	if (m_AlienShips.size() > 0)
	{
		for (int i=0; i < m_AlienShips.size(); i++)
		{
			//should it shoot?
			if (!m_AlienShips.at(i)->GetFinished())
			{
				int shootTimer = m_AlienShips.at(i)->GetShootTimer();
				shootTimer += a_fDeltaTick*1000;
				m_AlienShips.at(i)->SetShootTimer(shootTimer);
				int random = rand() % 300 + 1;
				if( m_AlienShips.at(i)->GetShootTimer() > random*1000)
				{
					CreateAlientProjectile( m_AlienShips.at(i)->GetX(), m_AlienShips.at(i)->GetY() );
					m_AlienShips.at(i)->SetShootTimer(0);
				}
			}

			//render the ship
			m_AlienShips.at(i)->Process(a_fDeltaTick);

			//is it ready to be deleted?
			if (m_AlienShips.at(i)->GetFinished())//todo: rename to isfinished its a god damn bool.
			{
				delete m_AlienShips.at(i);
				m_AlienShips.erase(m_AlienShips.begin() + i);
			}
		}
	}


	if (m_PowerUps.size() > 0)
	{
		for (int i=0; i < m_PowerUps.size(); i++)
		{
			m_PowerUps.at(i)->Process(a_fDeltaTick);
			if (m_PowerUps.at(i)->IsFinished())
			{
				delete m_PowerUps.at(i);
				m_PowerUps.erase(m_PowerUps.begin() + i);
			}
		}
	}


	ProcessEndGame();
}


void CLevel::ProcessTimeEvents(float a_fDeltaTick)
{
	
	//int randomTime = rand() %(20-10)+10;
	//int test = (randomTime*1000)+m_iTimeLastPowerUp;
	if (m_iTimePassed > (5*1000)+m_iTimeLastPowerUp)
	{
		int randomType = rand() % 2 + 1; //TODO: Fix random gen
		int randomX = rand() % 780 + 40;
		CreatePowerUp(randomType, randomX);
		
		m_iTimeLastPowerUp = m_iTimePassed;
	}

	if (m_iTimePassed > 0 && m_WavesFinished.at(WAVE_1) == false)
	{
		std::vector<float> waveXPath;
		std::vector<float> waveYPath;

		waveXPath.push_back(50);//1
		waveYPath.push_back(50);
		waveXPath.push_back(750);//2
		waveYPath.push_back(50);
		waveXPath.push_back(750);//3
		waveYPath.push_back(150);
		waveXPath.push_back(50);//4
		waveYPath.push_back(150);

		CreateAlienShip(50, 60, waveXPath, waveYPath);
		CreateAlienShip(50, 150, waveXPath, waveYPath);
		CreateAlienShip(50, 240, waveXPath, waveYPath);
		CreateAlienShip(50, 330, waveXPath, waveYPath);

		m_WavesFinished.at(WAVE_1) = true;
	}


	if (m_iTimePassed > (6*1000) && m_WavesFinished.at(WAVE_2) == false)//|| m_AlienShips.size() == 0)
	{
		std::vector<float> wave1XPath;
		std::vector<float> wave1YPath;
		wave1XPath.push_back(-40);//1
		wave1YPath.push_back(-40);
		wave1XPath.push_back(200);//2
		wave1YPath.push_back(150);
		wave1XPath.push_back(650);//3
		wave1YPath.push_back(150);
		wave1XPath.push_back(850);//4
		wave1YPath.push_back(-40);

		CreateAlienShip(-100, -100, wave1XPath, wave1YPath);

		std::vector<float> wave1XPathb;
		std::vector<float> wave1YPathb;
		wave1XPathb.push_back(840);//1
		wave1YPathb.push_back(-40);
		wave1XPathb.push_back(650);//2
		wave1YPathb.push_back(250);
		wave1XPathb.push_back(150);//3
		wave1YPathb.push_back(250);
		wave1XPathb.push_back(-40);//4
		wave1YPathb.push_back(-40);

		CreateAlienShip(850, -50, wave1XPathb, wave1YPathb);

		m_WavesFinished.at(WAVE_2) = true;
	}

	if (m_iTimePassed > (9*1000) && m_WavesFinished.at(WAVE_2) == false)
	{
		std::vector<float> waveXPath;
		std::vector<float> waveYPath;

		waveXPath.push_back(50);//1
		waveYPath.push_back(-40);
		waveXPath.push_back(50);//2
		waveYPath.push_back(300);
		CreateAlienShip(50, -50, waveXPath, waveYPath);

		waveXPath.clear();
		waveYPath.clear();
		waveXPath.push_back(200);//1
		waveYPath.push_back(-40);
		waveXPath.push_back(200);//2
		waveYPath.push_back(300);
		CreateAlienShip(200, -50, waveXPath, waveYPath);

		waveXPath.clear();
		waveYPath.clear();
		waveXPath.push_back(400);//1
		waveYPath.push_back(-40);
		waveXPath.push_back(400);//2
		waveYPath.push_back(300);
		CreateAlienShip(400, -50, waveXPath, waveYPath);

		std::vector<float> waveXPathc;
		std::vector<float> waveYPathc;
		waveXPathc.push_back(600);//1
		waveYPathc.push_back(-40);
		waveXPathc.push_back(600);//2
		waveYPathc.push_back(300);
		CreateAlienShip(600, -50, waveXPathc, waveYPathc);

		m_WavesFinished.at(WAVE_2) = true;
	}

	if (m_iTimePassed > (12*1000) && m_WavesFinished.at(WAVE_3) == false)
	{
		std::vector<float> waveXPath;
		std::vector<float> waveYPath;

		waveXPath.push_back(-40);//1
		waveYPath.push_back(350);
		waveXPath.push_back(850);//2
		waveYPath.push_back(350);
		CreateAlienShip(-50, 50, waveXPath, waveYPath);

		waveXPath.clear();
		waveYPath.clear();
		waveXPath.push_back(840);//1
		waveYPath.push_back(450);
		waveXPath.push_back(-40);//2
		waveYPath.push_back(450);
		CreateAlienShip(850, 50, waveXPath, waveYPath);

		m_WavesFinished.at(WAVE_3) = true;
	}

	if (m_iTimePassed > (14*1000) && m_WavesFinished.at(WAVE_4) == false)
	{
		std::vector<float> waveXPath;
		std::vector<float> waveYPath;

		waveXPath.push_back(840);//1
		waveYPath.push_back(100);
		waveXPath.push_back(-40);//2
		waveYPath.push_back(100);
		CreateAlienShip(850, 100, waveXPath, waveYPath);

		waveXPath.clear();
		waveYPath.clear();
		waveXPath.push_back(840);//1
		waveYPath.push_back(200);
		waveXPath.push_back(-40);//2
		waveYPath.push_back(200);
		CreateAlienShip(850, 200, waveXPath, waveYPath);

		m_WavesFinished.at(WAVE_4) = true;
	}
}


void CLevel::ProcessCollisions(float a_fDeltaTick)
{
	m_pPlayer->CheckOutOfBounds();

	ProcessAlienProjectileCollisions();

	ProcessPlayerProjectileCollisions();

	ProcessPowerUpCollisions();
}


void CLevel::ProcessPlayerProjectileCollisions()
{
	if (m_PlayerProjectiles.size() > 0) //for each bullet if there is any
	{
		for (int iProjectile=0; iProjectile < m_PlayerProjectiles.size(); iProjectile++)
		{
			/*
			CBoundingRect bulletHitbox = m_PlayerProjectiles.at(i)->GetBoundingRect();
			if(alienShipHitbox.Contains(bulletHitbox))
			{
				m_pAlien->SetMoving(false);
				//m_PlayerProjectiles.at(i)->m_alive = false;
			}*/
			float bx = m_PlayerProjectiles.at(iProjectile)->GetX();
			float by = m_PlayerProjectiles.at(iProjectile)->GetY();
			
			if (m_AlienShips.size() > 0) //for each alien ship if there is any
			{
				for (int iAlien=0; iAlien<m_AlienShips.size(); iAlien++)
				{
					if (!m_AlienShips.at(iAlien)->GetDestroyed())
					{
						//TODO: move this into a class like bounding rect to get object sizes // DONE but keep as a reference. <------------------------------------------ read this before copying. 
						//use ProcessAlienProjectileCollisions() as a better way for collision dectection using bounding rect class
						float ax1 = m_AlienShips.at(iAlien)->GetX()-32;//GetX is in the middle of spite so go back half of sprite size
						float ay1 = m_AlienShips.at(iAlien)->GetY()-32;
						float ax2 = m_AlienShips.at(iAlien)->GetX()+32;
						float ay2 = m_AlienShips.at(iAlien)->GetY()+32;

						//TODO: is this overkill? maybe loop thru every third pixel instead
						for (int ix=bx-10; ix<bx+10; ix++) //for any pixel inside projectile
						{
							for (int iy=by-10; iy<by+10; iy++)
							{
								if( //check each pixle to see if any are inside current alien ship
									(ix > ax1 && ix < ax2) &&
									(iy > ay1 && iy < ay2)
								  )
								{
									m_AlienShips.at(iAlien)->SetState(CAlienShip::SHIPSTATE_DESTORYED);
									m_PlayerProjectiles.at(iProjectile)->SetFinished(true);
								}
							}
						}
					}
				}
			}
		}
	}
}


void CLevel::ProcessAlienProjectileCollisions()
{
	if (m_AlienProjectiles.size() > 0) //for each bullet if there is any
	{
		for (int iProjectile=0; iProjectile < m_AlienProjectiles.size(); iProjectile++)
		{
			CBoundingRect& playerBox = m_pPlayer->GetBoundingRect();
			
			if( playerBox.Contains2(m_AlienProjectiles.at(iProjectile)->GetBoundingRect()) )
			{
				m_AlienProjectiles.at(iProjectile)->SetFinished(true);
				m_pPlayerHealth->SubtractHealth(1);
			}
		}
	}
}


void CLevel::ProcessPowerUpCollisions()
{
	if (m_PowerUps.size() > 0) //for each power up if there is any
	{
		for (int iPup=0; iPup < m_PowerUps.size(); iPup++)
		{
			CBoundingRect& playerBox = m_pPlayer->GetBoundingRect();

			if( playerBox.Contains2(m_PowerUps.at(iPup)->GetBoundingRect()) )
			{
				//m_PowerUps.at(iPup)->DoPowerUpLogic();
				switch (m_PowerUps.at(iPup)->GetPowerUpType())
				{
					case EPowerUpType::PU_HEAL:
						m_pPlayerHealth->IncreaseHealth(1);
						break;
					case EPowerUpType::PU_SPEED:
						m_pPlayer->SetMoveSpeed(500.0f);
						break;
					default:
						//nothing, probably havent defined the power up type if it gets here
						break;
				}
				m_PowerUps.at(iPup)->SetFinished(true);
			}
			if(m_PowerUps.at(iPup)->GetY() > 800)//should really get the level size from 1 place not a hard code it 
			{
				m_PowerUps.at(iPup)->SetFinished(true);
			}
		}
	}
}


void CLevel::ProcessEndGame()
{
	// Check the end game here and change the state apprpriately
	//CGame::GetInstance().SetGameOver(); 

	if(m_pPlayerHealth->GetCurrentHealth() <= 0)
	{
		//show gameover sprite; good enough lol. no need to exit window, and we dont have a menu atm to go back to.
		
	}
}



CPlayerShip* CLevel::GetPlayerShip()
{
	return m_pPlayer;
}


void CLevel::CreateProjectile()
{

	CBullet* bullet = new CBullet();
	bullet->Initialise(m_pPlayer->GetX(), m_pPlayer->GetY());
	
	m_PlayerProjectiles.push_front(bullet);
}


void CLevel::CreateAlientProjectile(int a_x, int a_y)
{

	CAlienProjectile1* alienProjectile1 = new CAlienProjectile1();
	alienProjectile1->Initialise(a_x, a_y); 
	
	m_AlienProjectiles.push_front(alienProjectile1);
}




void CLevel::CreateAlienShip(int a_iCreateXpos, int a_iCreateYpos, std::vector<float> a_xPaths, std::vector<float> a_yPaths)
{
	CAlienShip* alienShip = new CAlienShip();
	alienShip->Initialise(a_iCreateXpos, a_iCreateYpos, a_xPaths, a_yPaths);
	//alienShip->SetPath(a_xPaths, a_yPaths);
	
	m_AlienShips.push_front(alienShip);
}


void CLevel::SetPlayerShooting(bool a_status)
{
	m_bPlayerShooting = a_status;
}


void CLevel::CreatePowerUp(int a_iType, int a_iXPos)
{
	if (a_iType == 1){//TODO: use enum not a number
		CPowerUp* power = new CHealthPowerUp();
		power->Initialise(a_iXPos, 0);
		power->SetPowerUpType(0);
		m_PowerUps.push_front(power);
	}
	else if (a_iType == 2)
	{
		CPowerUp* power2 = new CSpeedPowerUp();
		power2->Initialise(a_iXPos, 0);
		power2->SetPowerUpType(1);
		m_PowerUps.push_front(power2);
	}
}



