// GPE.cpp : Defines the entry point for the application.
//
#include "stdafx.h"

//Local Includes
#include "Game.h"
#include "Clock.h"
#include "GPEUtils.h"


#define WINDOW_CLASS_NAME L"GPE FRAMEWORK"

LRESULT CALLBACK WindowProc(HWND a_hWnd, UINT a_uiMsg, WPARAM a_wParam, LPARAM a_lParam)
{
	// Windows is an event driven environment. 

	switch (a_uiMsg)
	{
		// You can read the documentation on windows messages. These could provide you with ideas for your own games. 
		// You may have to read the documentation (depending on who you are working with). You may be able to ask other people to save time. 
		// If you discover something interesting in this area then you should share it with your peers. 

		case WM_CREATE:
		{
			// This is the message we receive from windows when the window is first created. 
			// We may have to do some initialising here for our game (if it is related to the windows create message)!
			return(0);
		}
		break;
		case WM_KEYDOWN:
		{
			CGame::GetInstance().ProcessKeyDownMessage(a_wParam); 
			break;
		}
		case WM_KEYUP:
		{
			CGame::GetInstance().ProcessKeyUpMessage(a_wParam); 
			break;
		}
		case WM_DESTROY:
		{
			// kill the application, this sends a WM_QUIT message
			PostQuitMessage(0);
			return(0);
		}
		break;
	
		default:break;
	} 
 	
	return (DefWindowProc(a_hWnd, a_uiMsg, a_wParam, a_lParam));
}

HWND CreateAndRegisterWindow(HINSTANCE a_hInstance, int a_iWidth, int a_iHeight, LPCWSTR a_pcTitle)
{
	WNDCLASSEX winclass;

	winclass.cbSize			= sizeof(WNDCLASSEX);
	winclass.style			= CS_HREDRAW | CS_VREDRAW;
	winclass.lpfnWndProc	= WindowProc;
	winclass.cbClsExtra		= 0;
	winclass.cbWndExtra		= 0;
	winclass.hInstance		= a_hInstance;
	winclass.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
	winclass.hCursor		= LoadCursor(NULL, IDC_ARROW);
	winclass.hbrBackground	= static_cast<HBRUSH> (GetStockObject(NULL_BRUSH));
	winclass.lpszMenuName	= NULL;
	winclass.lpszClassName	= WINDOW_CLASS_NAME;
	winclass.hIconSm		= LoadIcon(NULL, IDI_APPLICATION);

	if (!RegisterClassEx(&winclass))
	{
		assert(false=="Failed to Register Class");
		return (0);
	}

	HWND hwnd = CreateWindowEx(	NULL,
								WINDOW_CLASS_NAME,
								a_pcTitle,
								WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_VISIBLE, 
								CW_USEDEFAULT, CW_USEDEFAULT,
								a_iWidth, a_iHeight,
								NULL,
								NULL,
								a_hInstance,
								NULL);
	
	if (!hwnd)
	{
		// Failed to create.
		assert(false=="Failed to Create Window");
		return (0);
	}

	return (hwnd);
}

int WINAPI WinMain(HINSTANCE a_hInstance, HINSTANCE a_hPrevInstance, LPSTR a_lpCmdline, int a_iCmdshow)
{
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	const int kiWidth = 800;
	const int kiHeight = 800;

	HWND hwnd = CreateAndRegisterWindow(a_hInstance, kiWidth, kiHeight, L"SPACE BOMBER");//TODO: name the game!

	CGame& Game = CGame::GetInstance();

	if (!Game.Initialise(a_hInstance, hwnd, kiWidth, kiHeight))
	{
		assert (false=="Failed to initialise the game."); 
		return (0);
	}

	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			Game.ExecuteOneFrame();
		}
	}

	CGame::DestroyInstance();

	return (static_cast<int>(msg.wParam));
}
