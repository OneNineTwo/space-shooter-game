#include "StdAfx.h"
#include "resource.h"
#include "AnimatedSprite.h"
#include "PlayerShip.h"


CPlayerShip::CPlayerShip(void)
{
}


CPlayerShip::~CPlayerShip(void)
{
	Deinitialise();
}


bool CPlayerShip::Deinitialise()
{
	return (true);
}


bool CPlayerShip::Initialise()
{
	for (int i=0; i<SHIPSTATE_TOTALSTATES; ++i)
	{
		m_bMoving[i] = false;
	}

	m_eCurrentState = SHIPSTATE_STILL;
	m_fX = 400;
	m_fY = 600;
	m_fMoveSpeed = 300.0f;
	m_iShootTimer = 0;

	int iMask = IDB_PLAYERSHIPMASK;
	int iSprite = IDB_PLAYERSHIP;


	m_pAnim[SHIPSTATE_STILL] = new CAnimatedSprite();
	assert(m_pAnim[SHIPSTATE_STILL]);
	m_pAnim[SHIPSTATE_STILL]->Initialise(iSprite, iMask);
	m_pAnim[SHIPSTATE_STILL]->SetWidth(95);
	m_pAnim[SHIPSTATE_STILL]->SetSpeed(0.1f);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(95);

	m_pAnim[SHIPSTATE_LEFT] = new CAnimatedSprite();
	assert(m_pAnim[SHIPSTATE_LEFT]); 
	m_pAnim[SHIPSTATE_LEFT]->Initialise(iSprite, iMask);
	m_pAnim[SHIPSTATE_LEFT]->SetWidth(95);
	m_pAnim[SHIPSTATE_LEFT]->SetSpeed(0.1f);
	m_pAnim[SHIPSTATE_LEFT]->AddFrame(0);

	m_pAnim[SHIPSTATE_RIGHT] = new CAnimatedSprite();
	assert(m_pAnim[SHIPSTATE_RIGHT]); 
	m_pAnim[SHIPSTATE_RIGHT]->Initialise(iSprite, iMask);
	m_pAnim[SHIPSTATE_RIGHT]->SetWidth(95);
	m_pAnim[SHIPSTATE_RIGHT]->SetSpeed(0.1f);
	m_pAnim[SHIPSTATE_RIGHT]->AddFrame(190);

	return (true);
}


void CPlayerShip::Render()
{
	m_pAnim[m_eCurrentState]->Render();
}


void CPlayerShip::Process(float a_fDeltaTick)
{
	m_eCurrentState = SHIPSTATE_STILL;

	if (m_bMoving[SHIPSTATE_LEFT])
	{
		m_fX -= m_fMoveSpeed * a_fDeltaTick;
		m_eCurrentState = SHIPSTATE_LEFT;
	}
	else if (m_bMoving[SHIPSTATE_RIGHT])
	{
		m_fX += m_fMoveSpeed * a_fDeltaTick;
		m_eCurrentState = SHIPSTATE_RIGHT;
	}

	if (m_bMoving[SHIPSTATE_UP])
	{
		m_fY -= m_fMoveSpeed * a_fDeltaTick;
	}
	else if (m_bMoving[SHIPSTATE_DOWN])
	{
		m_fY += m_fMoveSpeed * a_fDeltaTick;
	}

	m_pAnim[m_eCurrentState]->Process(a_fDeltaTick);

	m_pAnim[m_eCurrentState]->SetX(m_fX);
	m_pAnim[m_eCurrentState]->SetY(m_fY);

}


CBoundingRect CPlayerShip::GetBoundingRect()
{
	m_BoundingRect.x1 = m_fX - 10;
	m_BoundingRect.x2 = m_fX + 20;
	m_BoundingRect.y1 = m_fY - 50;
	m_BoundingRect.y2 = m_fY + 60;

	return m_BoundingRect;
}



void CPlayerShip::SetState(EShipState a_eState, bool a_bMoving)
{
	m_bMoving[a_eState] = a_bMoving;
}


float CPlayerShip::GetX()
{
	return m_fX;
}


float CPlayerShip::GetY()
{
	return m_fY;
}


void CPlayerShip::CheckOutOfBounds() //should move to be handled by level
{
	m_PushBack = false;//this property does nothing atm..but maybe we can find a use lol, like do out of bounds dynamic to the level size not fixed.
	if(m_fX < 30)
	{
		m_fX = 30; 
		m_PushBack = true;
	}
	else if(m_fX > 800-35)
	{
		m_fX = 800-35;
		m_PushBack = true;
	}
	if(m_fY < 70)
	{
		m_fY = 70; 
		m_PushBack = true; 
	}
	else if(m_fY > 800-95)
	{
		m_fY = 800-95;
		m_PushBack = true; 
	}
}


int CPlayerShip::GetShootTimer()
{
	return m_iShootTimer;
}

void CPlayerShip::SetShootTimer(int a_iTime)
{
	m_iShootTimer = a_iTime;
}


void CPlayerShip::SetMoveSpeed(int a_fSpeed)
{
	m_fMoveSpeed = a_fSpeed;
}