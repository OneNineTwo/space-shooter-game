#include "stdafx.h"
#include "resource.h"
#include "Terrain.h"

#include "Sprite.h"
#include "resource.h"

CTerrain::CTerrain(void)
{
	m_fX = 200;
	m_fY = 200;
}


CTerrain::~CTerrain(void)
{
	Deinitialise();
}


bool CTerrain::Deinitialise()
{
	return (true);
}

bool CTerrain::Initialise()
{
	m_pTerrainSprite = new CSprite();
	m_pTerrainSprite->Initialise(IDB_TERRIAN, IDB_TERRAINMASK);
	m_pTerrainSprite->SetX(m_fX);
	m_pTerrainSprite->SetY(m_fY);

	return (true);
}

void CTerrain::Render()
{
	m_pTerrainSprite->Render();
}

void CTerrain::Process(float a_fDeltaTick)
{
	float fly_speed = 10;
	m_fY += fly_speed * a_fDeltaTick;
	m_pTerrainSprite->SetY(m_fY);
}