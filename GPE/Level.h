#pragma once

#include <WinDef.h>
#include <deque>
#include <vector>

class CSoundFX;
class CTerrain;
class CPlayerHealth;
class CBullet;
class CPlayerShip;
class CAlienShip;
class CAlienProjectile1;
class CPowerUp;
class CSprite;


class CLevel
{
	public:

		enum EPowerUpType
		{
			PU_INVALID = -1,
			PU_HEAL,
			PU_SPEED,
			PU_TOTALTYPES,
		};

		enum EWaves
		{
			WAVE_INVALID = -1,
			WAVE_1,
			WAVE_2,
			WAVE_3,
			WAVE_4,
			WAVE_TOTALWAVES,
		};


		CLevel();
		virtual ~CLevel();

		virtual bool Deinitialise();
		virtual bool Initialise(int a_iWidth, int a_iHeight);

		virtual void Render();

		virtual void Process(float a_fDeltaTick);
		void Play();
		void ProcessEndGame();

		CPlayerShip* GetPlayerShip();

		void ProcessTimeEvents(float a_fDeltaTick);
		void ProcessCollisions(float a_fDeltaTick);
		void ProcessAlienProjectileCollisions();
		void ProcessPlayerProjectileCollisions();
		void ProcessPowerUpCollisions();

		void CreateProjectile();
		void CreateAlientProjectile(int a_x, int a_y);
		void CreateAlienShip(int a_iCreateXpos, int a_iCreateYpos);
		void CreateAlienShip(int a_iCreateXpos, int a_iCreateYpos, std::vector<float> a_xPaths, std::vector<float> a_yPaths);
		void SetPlayerShooting(bool a_status);
		void CreatePowerUp(int a_iType, int a_iXPos);

	protected:

		std::deque<CBullet*>				m_PlayerProjectiles;
		std::deque<CAlienProjectile1*>		m_AlienProjectiles;
		std::deque<CAlienShip*>				m_AlienShips;
		std::deque<CPowerUp*>				m_PowerUps;
		std::deque<bool>					m_WavesFinished;

		CTerrain							*m_pTerrain;
		CPlayerHealth						*m_pPlayerHealth;
		CPlayerShip							*m_pPlayer;
		CSprite								*m_gameover;

		int									m_iWidth;
		int									m_iHeight;
		bool								m_bPlayerShooting;
		int									m_iTimePassed;
		int									m_iTimeLastPowerUp;

};
