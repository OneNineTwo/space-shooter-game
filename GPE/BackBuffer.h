#pragma once

#include <Windows.h>

class CBackBuffer
{
	public:
		CBackBuffer();
		virtual ~CBackBuffer();

		virtual bool Initialise(HWND a_hWnd, int piWidth, int a_iHeight);

		HDC GetBFDC() const;

		int GetHeight() const;
		int GetWidth() const;

		void Clear();
		void Present();

	private:
		CBackBuffer(const CBackBuffer& a_rBackBuffer);
		CBackBuffer& operator= (const CBackBuffer& a_rBackBuffer);
		HWND		m_hWnd;
		HDC			m_hDC;
		HBITMAP		m_hSurface;
		HBITMAP		m_hOldObject;
		int			m_iWidth;
		int			m_iHeight;

};

