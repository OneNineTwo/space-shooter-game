#include "stdafx.h"
#include "Game.h"
#include "BackBuffer.h"
#include "AnimatedSprite.h"
#include "GPEUtils.h"

CAnimatedSprite::CAnimatedSprite() :
	m_fFrameSpeed(0.0f), 
	m_fTimeElapsed(0.0f),
	m_iCurrentSprite(0)
{
}

CAnimatedSprite::~CAnimatedSprite()
{
	Deinitialise();
}

bool CAnimatedSprite::Deinitialise()
{
	return (CSprite::Deinitialise());
}

bool CAnimatedSprite::Initialise(int a_iSpriteResourceID, int a_iMaskResourceID)
{
	m_bFinished = false;
	m_eAnimationType = AT_LOOP;
	m_bLoop = true;
	return (CSprite::Initialise(a_iSpriteResourceID, a_iMaskResourceID));
}

COLORREF colorRed = RGB(255, 0, 0);
HPEN RedPen = CreatePen(PS_SOLID, 2, colorRed);

void CAnimatedSprite::Render()
{
	int iTopLeftX	= m_vectorFrames[m_iCurrentSprite];
	int iTopLeftY	= 0;
	int iW			= GetFrameWidth();
	int iH			= GetHeight();
	int iX			= m_iX - (iW / 2);
	int iY			= m_iY - (iH / 2);
	COLORREF old_fCol, old_bCol;
	int old_bMode;

	old_fCol = SetTextColor(CGame::GetInstance().GetBackBuffer()->GetBFDC(), RGB(255, 0, 0));
	old_bCol = SetBkColor(CGame::GetInstance().GetBackBuffer()->GetBFDC(), RGB(0, 0, 0));
	old_bMode = SetBkMode(CGame::GetInstance().GetBackBuffer()->GetBFDC(), OPAQUE);


	HDC hSpriteDC = CGame::GetInstance().GetSpriteDC();
	HGDIOBJ hOldObj = SelectObject(hSpriteDC, m_hMask);
	BitBlt(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY, iW, iH, hSpriteDC, iTopLeftX, iTopLeftY, SRCAND);
	
	SelectObject(hSpriteDC, m_hSprite);
	BitBlt(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY, iW, iH, hSpriteDC, iTopLeftX, iTopLeftY, SRCPAINT);

	//SelectObject(CGame::GetInstance().GetBackBuffer()->GetBFDC(), RedPen);
	//MoveToEx(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY, NULL);
	//LineTo(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX + iW, iY);
	//LineTo(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX + iW, iY + iH);
	//LineTo(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY + iH);
	//LineTo(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY);
	
	SelectObject(hSpriteDC, hOldObj);

	SetTextColor(CGame::GetInstance().GetBackBuffer()->GetBFDC(), old_fCol);
	SetBkColor(CGame::GetInstance().GetBackBuffer()->GetBFDC(), old_bCol);
	SetBkMode(CGame::GetInstance().GetBackBuffer()->GetBFDC(), old_bMode);

}

void CAnimatedSprite::Process(float a_fDeltaTick)
{
	if(m_bLoop)
	{
		m_fTimeElapsed += a_fDeltaTick;
		if (m_fTimeElapsed >= m_fFrameSpeed && m_fFrameSpeed != 0.0f)
		{
			++m_iCurrentSprite;
			if (static_cast<size_t>(m_iCurrentSprite) >= m_vectorFrames.size())
			{
				if (m_eAnimationType == AT_ONCE)
				{
					m_bLoop = false;
					m_iCurrentSprite = m_vectorFrames.size()-1;
					m_bFinished = true;
				}
				else
				{
					m_iCurrentSprite = 0;
				}
			}
			m_fTimeElapsed = 0.0f;
		}
	}
	CSprite::Process(a_fDeltaTick);
}

void CAnimatedSprite::AddFrame(int a_iX)
{
	m_vectorFrames.push_back(a_iX);
}

void CAnimatedSprite::SetSpeed(float a_fSpeed)
{
	m_fFrameSpeed = a_fSpeed;
}

void CAnimatedSprite::SetWidth(int a_iW)
{
	m_iFrameWidth = a_iW;
}

int CAnimatedSprite::GetFrameWidth() const
{
	return (m_iFrameWidth);
}

void CAnimatedSprite::SetLoop(bool a_bStatus)
{
	m_bLoop = a_bStatus;
}

void CAnimatedSprite::SetAnimationType(EAnimationType a_eType)
{
	m_eAnimationType = a_eType;
}

bool CAnimatedSprite::GetFinished()
{
	return m_bFinished;
}