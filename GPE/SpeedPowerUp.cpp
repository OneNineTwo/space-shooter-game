#include "StdAfx.h"
#include "SpeedPowerUp.h"
#include "Sprite.h"
#include "resource.h"

CSpeedPowerUp::CSpeedPowerUp(void)
{

}


CSpeedPowerUp::~CSpeedPowerUp(void)
{
	Deinitialise();
}


bool CSpeedPowerUp::Initialise(int a_x, int a_y)
{
	m_bFinished = false;
	m_fMoveSpeed = 200.0f;
	m_fX = a_x;
	m_fY = a_y;

	int iMask = IDB_PUTIMEMASK;
	int iSprite = IDB_PUTIMESPRITE;

	m_pPowerUpSprite = new CSprite();
	m_pPowerUpSprite->Initialise(iSprite, iMask);
	m_pPowerUpSprite->SetX(m_fX);
	m_pPowerUpSprite->SetY(m_fY);

	return (true);
}


void CSpeedPowerUp::DoPowerUpLogic()
{
	
}