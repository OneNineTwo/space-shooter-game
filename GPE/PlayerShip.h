#pragma once


#include "BoundingRect.h"

class CAnimatedSprite;


class CPlayerShip
{

public:

	enum EShipState
	{
		SHIPSTATE_INVALID = -1,
		SHIPSTATE_STILL,
		SHIPSTATE_UP,
		SHIPSTATE_DOWN,
		SHIPSTATE_LEFT,
		SHIPSTATE_RIGHT,
		SHIPSTATE_TOTALSTATES = 5
	};

	enum EDirection
	{
		DIR_INVALID = -1,
		DIR_NONE,
		DIR_UP,
		DIR_DOWN,
		DIR_LEFT,
		DIR_RIGHT
	};

	enum EAnimationState
	{
		ANIMATION_STILL,
		ANIMATION_LEFT,
		ANIMATION_RIGHT
	};

public:

	CPlayerShip(void);
	virtual ~CPlayerShip(void);

	virtual bool Deinitialise();
	virtual bool Initialise();

	virtual void Render();
	virtual void Process(float a_fDeltaTick);

	void SetState(EShipState a_eState, bool a_bMoving);
	void CheckOutOfBounds();
	int GetShootTimer();
	void SetShootTimer(int a_iTime);
	CBoundingRect GetBoundingRect();
	float GetX();
	float GetY();
	void SetMoveSpeed(int a_fSpeed);

private:
	bool					m_bMoving[SHIPSTATE_TOTALSTATES];
	bool					m_PushBack;
	float					m_fX;
	float					m_fY;
	float					m_fMoveSpeed;
	CAnimatedSprite			*m_pAnim[SHIPSTATE_TOTALSTATES];
	CBoundingRect			m_BoundingRect; 
	EShipState   			m_eCurrentState;
	int						m_iShootTimer;
};

