#pragma once

#include "BoundingRect.h"
#include <vector>

class CAnimatedSprite;


class CAlienShip
{

public:

	enum EAlientShipState
	{
		SHIPSTATE_INVALID = -1,
		SHIPSTATE_STILL,
		SHIPSTATE_DESTORYED,
		SHIPSTATE_TOTALSTATES = 2
	};

public:

	CAlienShip(void);
	virtual ~CAlienShip(void);

	virtual bool Deinitialise();
	virtual bool Initialise(int a_iCreateXpos, int a_iCreateYpos, std::vector<float> a_xPaths, std::vector<float> a_yPaths);

	virtual void Render();
	virtual void Process(float a_fDeltaTick);
	void ProcessLevelBoundry();
	void ProcessMovement(float a_fDeltaTick);

	void SetState(EAlientShipState a_eState);
	CBoundingRect GetBoundingRect();
	void SetPath();
	void SetPath(std::vector<float> a_xPaths, std::vector<float> a_yPaths);
	void CalcMovment();
	void SetMoving(bool state);
	bool GetDestroyed();
	bool GetFinished();
	int GetShootTimer();
	void SetShootTimer(int a_iTime);

	float GetX();
	float GetY();

private:

	bool					m_bMoving[SHIPSTATE_TOTALSTATES];
	bool					m_PushBack;
	float					m_fX;
	float					m_fY;
	CAnimatedSprite			*m_pAnim[SHIPSTATE_TOTALSTATES];
	CBoundingRect			m_BoundingRect; 
	EAlientShipState		m_eCurrentState;
	std::vector<float>		m_vectorPathPointsX;
	std::vector<float>		m_vectorPathPointsY;
	int						m_iCurrentPathPointIndex;

	float m_fDistance;
	float m_fStartX;
	float m_fStartY;
	float m_fDirectionX;
	float m_fDirectionY;
	int m_iShootTimer;

	bool m_bDestroyed;
	bool m_bFinished;
};

