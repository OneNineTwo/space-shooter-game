#pragma once


#include "PowerUp.h"


class CHealthPowerUp : public CPowerUp
{
public:
	CHealthPowerUp(void);
	virtual ~CHealthPowerUp(void);
	
	void DoPowerUpLogic();
};

