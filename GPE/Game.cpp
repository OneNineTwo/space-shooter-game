#include "stdafx.h"
#include "Clock.h"
#include "BackBuffer.h"
#include "GPEUtils.h"
#include "Level.h"
#include "Game.h"
#include "Controller.h"
#include "SoundFX.h"

CGame* CGame::s_pGame = NULL;

CGame::CGame() :
		m_hApplicationInstance(NULL),
		m_hMainWindow(NULL),
		m_pClock(NULL),
		m_pBackBuffer(NULL),
		m_pController(NULL),
		m_pLevel(NULL)
{
}

CGame::~CGame()
{
	delete m_pBackBuffer;
	m_pBackBuffer = NULL;
	delete m_pLevel; 
	m_pLevel = NULL;
	delete m_pClock;
	m_pClock = NULL;
	delete m_pController; 
	m_pController = NULL; 
	delete m_pSoundFX; 
	m_pSoundFX = NULL; 
}

bool CGame::Initialise(HINSTANCE a_hInstance, HWND a_hWnd, int a_iWidth, int a_iHeight)
{
	m_hApplicationInstance = a_hInstance;
	assert(m_hApplicationInstance);
	m_hMainWindow = a_hWnd;
	assert(m_hMainWindow); 
	m_hSpriteDC = CreateCompatibleDC(NULL);
	assert(m_hSpriteDC); 
	m_pBackBuffer = new CBackBuffer();
	assert(m_pBackBuffer);
	m_pBackBuffer->Initialise(a_hWnd, a_iWidth, a_iHeight);
	m_pLevel = new CLevel();
	assert(m_pLevel);
	m_pController = new CController(); 
	assert(m_pController);
	m_pClock = new CClock; 
	assert(m_pClock); 
	m_pSoundFX = new CSoundFX(a_hInstance); 
	assert(m_pSoundFX); 
	if (!m_pLevel->Initialise(a_iWidth, a_iHeight))
	{
		// Initialise level specific information here.
		// This could be points per level, random encounters for the level
	}
	ShowCursor(false);
	return (true);
}

void CGame::Draw()
{
	m_pBackBuffer->Clear();
	m_pLevel->Render();
	m_pBackBuffer->Present();
	m_pSoundFX->Play(); 
}

void CGame::Process(float a_fDeltaTick)
{
	m_pLevel->Process(a_fDeltaTick);
	m_pSoundFX->Process(a_fDeltaTick); 
}

void CGame::ExecuteOneFrame()
{
	float fDT = m_pClock->GetDeltaTick();
	Process(fDT);
	Draw();
	m_pClock->Process();

	Sleep(1);
}

CGame& CGame::GetInstance()
{
	if (NULL == s_pGame)
	{
		assert(NULL == s_pGame);
		s_pGame = new CGame();
		assert(s_pGame); 
	}
	return (*s_pGame);
}

void CGame::DestroyInstance()
{
	assert(s_pGame != NULL);
	if(s_pGame != NULL)
	{
		assert(s_pGame != NULL);
		delete s_pGame;
		s_pGame = NULL;
	}
}

CBackBuffer* CGame::GetBackBuffer()
{
	return (m_pBackBuffer);
}

HINSTANCE CGame::GetAppInstance()
{
	return (m_hApplicationInstance);
}

HWND CGame::GetWindow()
{
	return (m_hMainWindow);
}

HDC CGame::GetSpriteDC()
{
	return (m_hSpriteDC);
}

CLevel* CGame::GetLevel()
{
	return (m_pLevel);
}

void CGame::ProcessKeyDownMessage(char a_wParam)
{
	m_pController->ProcessKeyDownMessage(a_wParam); 
}

void CGame::ProcessKeyUpMessage(char a_wParam)
{
	m_pController->ProcessKeyUpMessage(a_wParam); 
}

CSoundFX* CGame::GetSoundFXPlayer()
{
	return m_pSoundFX;
}