#pragma once

class CSprite;

class CTerrain
{
public:
	CTerrain(void);
	virtual ~CTerrain(void);

	virtual bool Deinitialise();
	virtual bool Initialise();

	virtual void Render();
	virtual void Process(float a_fDeltaTick);

private:
	float		m_fX;
	float		m_fY;
	CSprite		*m_pTerrainSprite;
};

