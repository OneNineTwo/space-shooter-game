#pragma once

#include "windows.h"

class CSprite
{
	public:
		CSprite();
		virtual ~CSprite();

		virtual bool Deinitialise();
		virtual bool Initialise(int a_iResourceID, int a_iMaskResourceID);

		virtual void Render();
		virtual void Process(float a_fDeltaTick);

		int GetWidth() const;
		int GetHeight() const;
		int GetSpriteX();
		int GetSpriteY();

		void SetX(int a_iX);
		void SetY(int a_iY);

		void SetCustomWidth(int a_iX);
		void SetCustomHeight(int a_iY);
		void SetSpriteX(int a_iX);
		void SetSpriteY(int a_iY);
		

	protected:
		int m_iX;
		int m_iY;
		int m_iCustomWidth;
		int m_iCustomHeight;
		int m_iSpriteX;//x position on the bmp sprite map (if there are more than 1 sprite frames to be used)
		int m_iSpriteY;
	
		HBITMAP m_hSprite;
		HBITMAP m_hMask;

		BITMAP m_BitmapSprite;
		BITMAP m_BitmapMask;
};


