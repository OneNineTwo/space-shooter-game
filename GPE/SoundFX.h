#pragma once

#include <windef.h>

class CSoundFX
{
public:

		enum ESoundFXPlay
		{
			INVALIDSOUND = -1,
			NOSOUND,
			BEAMSOUND,
			ENERGIZESOUND,
			WARPSOUND,
			TELEPORTSOUND
		};

		CSoundFX(HINSTANCE a_hInstance);
		~CSoundFX(void);

		void Play(); 
		void Process(float a_fDeltaTick);
		void SetSoundFXState(ESoundFXPlay a_eSoundFXPlay);

	private:
		void PlayTeleportSound();
		void PlayWarpSound();
		void PlayEnergizeSound(); 
		void PlayBeamSound(); 

	private:
		ESoundFXPlay	m_eSoundFXPlay;
		bool			m_bPlaying;
		HINSTANCE		m_hInstance;
		float			m_fSoundRepeat;
		float			m_fTimeElapsed;

};

