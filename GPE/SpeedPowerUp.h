#pragma once


#include "PowerUp.h"


class CSpeedPowerUp : public CPowerUp
{
public:
	CSpeedPowerUp(void);
	virtual ~CSpeedPowerUp(void);

	bool Initialise(int a_x, int a_y);
	void DoPowerUpLogic();
};

