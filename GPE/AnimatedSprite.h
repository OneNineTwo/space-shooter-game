#pragma once

#include <vector>
#include "windows.h"
#include "Sprite.h"


class CAnimatedSprite : public CSprite
{
	public:
		enum EAnimationType
		{
			AT_LOOP,
			AT_ONCE
		};

	public:
		CAnimatedSprite();
		virtual ~CAnimatedSprite();

		virtual bool Deinitialise();
		virtual bool Initialise(int a_iResourceID, int a_iMaskResourceID);

		virtual void Render();
		virtual void Process(float a_fDeltaTick);

		void AddFrame(int a_iX);
		void SetSpeed(float a_fSpeed);
		void SetLoop(bool a_bStatus);
		void SetAnimationType(EAnimationType a_eType);

		void SetWidth(int a_iW);
		int GetFrameWidth() const;
		bool GetFinished();

	private:
		std::vector<int>	m_vectorFrames;
		int					m_iCurrentSprite;
		int					m_iFrameWidth;
		float				m_fFrameSpeed;
		float				m_fTimeElapsed;
		bool				m_bLoop;
		bool				m_eAnimationType;

		bool				m_bFinished;
};

