#include "StdAfx.h"
#include "resource.h"
#include "AnimatedSprite.h"
#include "AlienShip.h"
#include <cmath>
#include <math.h>

const float CONST_FLY_SPEED = 200.0f;


CAlienShip::CAlienShip(void)
{
}


CAlienShip::~CAlienShip(void)
{
	Deinitialise();
}



bool CAlienShip::Deinitialise()
{
	return (true);
}


bool CAlienShip::Initialise(int a_iCreateXpos, int a_iCreateYpos, std::vector<float> a_xPaths, std::vector<float> a_yPaths)
{
	m_bFinished = false;
	m_bDestroyed = false;

	m_eCurrentState = SHIPSTATE_STILL;
	m_fX = a_iCreateXpos;
	m_fY = a_iCreateYpos;
	m_iCurrentPathPointIndex = 0;
	m_iShootTimer = 0;

	m_vectorPathPointsX = a_xPaths;
	m_vectorPathPointsY = a_yPaths;


	int iMask = IDB_ALIENSHIPMASK;
	int iSprite = IDB_ALIENSHIP;

	m_bMoving[SHIPSTATE_STILL] = false;

	m_pAnim[SHIPSTATE_STILL] = new CAnimatedSprite();//TODO: rename ship state. STILL is not really a good representation of what its doing lol
	assert(m_pAnim[SHIPSTATE_STILL]); 
	m_pAnim[SHIPSTATE_STILL]->Initialise(iSprite, iMask);
	m_pAnim[SHIPSTATE_STILL]->SetAnimationType(CAnimatedSprite::AT_LOOP);
	m_pAnim[SHIPSTATE_STILL]->SetWidth(64);
	m_pAnim[SHIPSTATE_STILL]->SetSpeed(0.1f);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(0);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(64);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(128);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(192);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(256);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(320);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(384);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(448);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(512);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(576);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(640);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(704);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(768);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(832);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(896);
	m_pAnim[SHIPSTATE_STILL]->AddFrame(960);
	

	iMask = IDB_EXPLOSIONMASK;
	iSprite = IDB_EXPLOSIONSPRITE;

	m_bMoving[SHIPSTATE_DESTORYED] = false;

	m_pAnim[SHIPSTATE_DESTORYED] = new CAnimatedSprite();
	assert(m_pAnim[SHIPSTATE_DESTORYED]); 
	m_pAnim[SHIPSTATE_DESTORYED]->Initialise(iSprite, iMask);
	m_pAnim[SHIPSTATE_DESTORYED]->SetAnimationType(CAnimatedSprite::AT_ONCE);
	m_pAnim[SHIPSTATE_DESTORYED]->SetWidth(96);
	m_pAnim[SHIPSTATE_DESTORYED]->SetSpeed(0.1f);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(0);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(192);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(288);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(384);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(480);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(576);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(672);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(768);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(864);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(960);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(1056);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(1152);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(1248);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(1344);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(1440);
	m_pAnim[SHIPSTATE_DESTORYED]->AddFrame(1536);


	//SetPath();

	CalcMovment();

	return (true);
}


void CAlienShip::Render()
{
	m_pAnim[m_eCurrentState]->Render();
}


void CAlienShip::Process(float a_fDeltaTick)
{
	if(m_pAnim[m_eCurrentState]->GetFinished()) //if animation finished the ship should be finished. ready to be deleted
	{
		m_bFinished = true;
	}

	ProcessLevelBoundry();

	if (m_bMoving[m_eCurrentState])
	{
		ProcessMovement(a_fDeltaTick);
	}

	m_pAnim[m_eCurrentState]->SetX(m_fX);
	m_pAnim[m_eCurrentState]->SetY(m_fY);

	m_pAnim[m_eCurrentState]->Process(a_fDeltaTick);
}


void CAlienShip::ProcessLevelBoundry()
{
	if (m_fX < -50)
	{
		m_fX = -50;
	}
	if (m_fX > 850)
	{
		m_fX = 850;
	}
	if (m_fY < -50)
	{
		m_fY = -50;
	}
	if (m_fY > 850)
	{
		m_fY = 850;
	}
}


void CAlienShip::ProcessMovement(float a_fDeltaTick)
{
	m_fX += m_fDirectionX * CONST_FLY_SPEED * a_fDeltaTick;
	m_fY += m_fDirectionY * CONST_FLY_SPEED * a_fDeltaTick;

	float dx = m_fX-m_fStartX;
	float dy = m_fY-m_fStartY;
	float newDistance = sqrtf(dx*dx+dy*dy);

	if(newDistance >= m_fDistance)
	{
		m_iCurrentPathPointIndex++;
		if (m_iCurrentPathPointIndex >= m_vectorPathPointsX.size())
		{
			m_iCurrentPathPointIndex = 0;
		}
		CalcMovment();
	}
}


void CAlienShip::CalcMovment()
{
	m_fStartX = m_fX;
	m_fStartY = m_fY;
	float endXpoint = m_vectorPathPointsX.at(m_iCurrentPathPointIndex);
	float endYpoint = m_vectorPathPointsY.at(m_iCurrentPathPointIndex);
	float dx = endXpoint-m_fStartX;
	float dy = endYpoint-m_fStartY;
	m_fDistance = sqrtf(dx*dx+dy*dy);
	m_fDirectionX = dx / m_fDistance;
	m_fDirectionY = dy / m_fDistance;
	m_bMoving[m_eCurrentState] = true;
}


void CAlienShip::SetState(EAlientShipState a_eState)
{
	m_eCurrentState = a_eState;
	if (m_eCurrentState == SHIPSTATE_DESTORYED)
	{
		m_bDestroyed = true;
	}
}


CBoundingRect CAlienShip::GetBoundingRect()
{
	m_BoundingRect.y1 = m_fY - 32;
	m_BoundingRect.x1 = m_fX - 32;
	m_BoundingRect.x2 = m_BoundingRect.x1 + 32;
	m_BoundingRect.y2 = m_BoundingRect.y1 + 32;
	return m_BoundingRect;
}


float CAlienShip::GetX()
{
	return m_fX;
}


float CAlienShip::GetY()
{
	return m_fY;
}


void CAlienShip::SetMoving(bool state)
{
	m_bMoving[m_eCurrentState] = state;
}


void CAlienShip::SetPath()//dont use this anymore but kept as an example
{
	//TODO: convert these x and y points into a struct
	m_vectorPathPointsX.push_back(50);//1
	m_vectorPathPointsY.push_back(50);

	m_vectorPathPointsX.push_back(750);//2
	m_vectorPathPointsY.push_back(50);

	m_vectorPathPointsX.push_back(750);//3
	m_vectorPathPointsY.push_back(150);

	m_vectorPathPointsX.push_back(50);//4
	m_vectorPathPointsY.push_back(150);
}

void CAlienShip::SetPath(std::vector<float> a_xPaths, std::vector<float> a_yPaths)//this is also not used, set the path when Initialiseing 
{
	m_vectorPathPointsX.clear();
	m_vectorPathPointsY.clear();
	m_vectorPathPointsX = a_xPaths;
	m_vectorPathPointsY = a_yPaths;
}

bool CAlienShip::GetDestroyed()
{
	return m_bDestroyed;
}

bool CAlienShip::GetFinished()
{
	return m_bFinished;
}

int CAlienShip::GetShootTimer()
{
	return m_iShootTimer;
}

void CAlienShip::SetShootTimer(int a_iTime)
{
	m_iShootTimer = a_iTime;
}