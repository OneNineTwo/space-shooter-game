#include "stdafx.h"
#include "resource.h"
#include "AnimatedSprite.h"
#include "BomberBoy.h"

const float CONST_WALK_SPEED = 100.0f;

CBomberBoy::CBomberBoy(float a_fX, float a_fY) :
	m_eFacing(FACING_FORWARD),
	m_eDirection(FACING_FORWARD), 
	m_fX(a_fX),
	m_fY(a_fY),
	m_PushBack(false)
{
}

CBomberBoy::~CBomberBoy()
{
	Deinitialise();
}

bool CBomberBoy::Deinitialise()
{
	return (true);
}

bool CBomberBoy::Initialise(EBomberBoyColour a_eColour)
{
	for (int i = INVALID_FACING + 1; i < MAX_FACING; ++i)
	{
		m_bWalking[i] = false;
	}

	int iMask = IDB_BOMBERBOYMASK;

	int iSprite = 0;

	if (a_eColour == BOMBERBOYCOLOUR_BLACK)
	{
		iSprite = IDB_BOMBERBOYBLACK;
	}
	else if (a_eColour == BOMBERBOYCOLOUR_WHITE)
	{
		iSprite = IDB_BOMBERBOYWHITE;
	}

	m_pAnim[FACING_FORWARD] = new CAnimatedSprite();
	assert(m_pAnim[FACING_FORWARD]); 
	m_pAnim[FACING_FORWARD]->Initialise(iSprite, iMask);
	m_pAnim[FACING_FORWARD]->SetWidth(32);
	m_pAnim[FACING_FORWARD]->SetSpeed(0.0f);
	m_pAnim[FACING_FORWARD]->AddFrame(0);
	m_pAnim[FACING_FORWARD]->AddFrame(32);
	m_pAnim[FACING_FORWARD]->AddFrame(64);
	m_pAnim[FACING_FORWARD]->AddFrame(32);

	m_pAnim[FACING_BACKWARD] = new CAnimatedSprite();
	assert(m_pAnim[FACING_BACKWARD]); 
	m_pAnim[FACING_BACKWARD]->Initialise(iSprite, iMask);
	m_pAnim[FACING_BACKWARD]->SetWidth(32);
	m_pAnim[FACING_BACKWARD]->SetSpeed(0.0f);
	m_pAnim[FACING_BACKWARD]->AddFrame(288);
	m_pAnim[FACING_BACKWARD]->AddFrame(320);
	m_pAnim[FACING_BACKWARD]->AddFrame(352);

	m_pAnim[FACING_LEFT] = new CAnimatedSprite();
	assert(m_pAnim[FACING_LEFT]); 
	m_pAnim[FACING_LEFT]->Initialise(iSprite, iMask);
	m_pAnim[FACING_LEFT]->SetWidth(32);
	m_pAnim[FACING_LEFT]->SetSpeed(0.0f);
	m_pAnim[FACING_LEFT]->AddFrame(192);
	m_pAnim[FACING_LEFT]->AddFrame(224);
	m_pAnim[FACING_LEFT]->AddFrame(256);

	m_pAnim[FACING_RIGHT] = new CAnimatedSprite();
	assert(m_pAnim[FACING_RIGHT]);
	m_pAnim[FACING_RIGHT]->Initialise(iSprite, iMask);
	m_pAnim[FACING_RIGHT]->SetWidth(32);
	m_pAnim[FACING_RIGHT]->SetSpeed(0.0f);
	m_pAnim[FACING_RIGHT]->AddFrame(96);
	m_pAnim[FACING_RIGHT]->AddFrame(128);
	m_pAnim[FACING_RIGHT]->AddFrame(160);

	m_eFacing = FACING_FORWARD;

	return (true);
}

void CBomberBoy::Render()
{
	m_pAnim[m_eFacing]->Render();
}

void CBomberBoy::Process(float a_fDeltaTick)
{
	m_pAnim[m_eFacing]->Process(a_fDeltaTick);

	if (m_bWalking[m_eFacing])
	{
		switch (m_eFacing)
		{
			case FACING_FORWARD:
			{
				m_fY += CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
			case FACING_BACKWARD:
			{
				m_fY -= CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
			case FACING_LEFT:
			{
				m_fX -= CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
			case FACING_RIGHT:
			{
				m_fX += CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
		}
	}

	PushBomberBoyBack();
	m_pAnim[m_eFacing]->SetX(static_cast<int>(m_fX));
	m_pAnim[m_eFacing]->SetY(static_cast<int>(m_fY));

}

CBoundingRect CBomberBoy::GetBoundingRectForNextFrame(float a_fDeltaTick)
{

	if (m_bWalking[m_eFacing])
	{
		switch (m_eFacing)
		{
			case FACING_FORWARD:
			{
				m_BoundingRect.y1 = (m_fY - 16) + CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
			case FACING_BACKWARD:
			{
				m_BoundingRect.y1 = (m_fY - 16) - CONST_WALK_SPEED * a_fDeltaTick;
				break; 
			}
			case FACING_LEFT:
			{
				m_BoundingRect.x1 = (m_fX - 16) - CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
			case FACING_RIGHT:
			{
				m_BoundingRect.x1 = (m_fX - 16) + CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
		}
	}
	PushBomberBoyBack();


	m_BoundingRect.x2 = m_BoundingRect.x1 + 32; 
	m_BoundingRect.y2 = m_BoundingRect.y1 + 32;
	
	return m_BoundingRect;
}


void CBomberBoy::PushBomberBoyBack()
{
	m_PushBack = false;
	if(m_fX < 31)
	{
		m_fX = 32; 
		m_PushBack = true; 
	}
	else if(m_fX > 340)
	{
		m_fX = 340;
		m_PushBack = true; 
	}
	if(m_fY < 31)
	{
		m_fY = 32; 
		m_PushBack = true; 
	}
	else if(m_fY > 320)
	{
		m_fY = 320;
		m_PushBack = true; 
	}
}

void CBomberBoy::SetWalkState(EFacing a_eDirection)
{
	m_eFacing = a_eDirection;
	Walk();
}

void CBomberBoy::Walk()
{
	m_pAnim[m_eFacing]->SetSpeed(0.1f);
	m_bWalking[m_eFacing] = true;
}

void CBomberBoy::SetStopWalkingState(EFacing a_eDirection)
{
	m_eDirection = a_eDirection;
	m_pAnim[a_eDirection]->SetSpeed(0.0f);
	m_bWalking[a_eDirection] = false;
}

void CBomberBoy::SetStopWalkingState()
{
	m_pAnim[m_eDirection]->SetSpeed(0.0f);
	m_bWalking[m_eFacing] = false;
}

void CBomberBoy::RandomiseLocation()
{
	this->m_fX = static_cast<float>(rand() % 300) + 30; 
	this->m_fY = static_cast<float>(rand() % 300) + 30; 
}

float CBomberBoy::GetX()
{
	return m_fX;
}

float CBomberBoy::GetY()
{
	return m_fY;
}