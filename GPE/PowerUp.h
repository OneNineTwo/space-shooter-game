#pragma once


#include "BoundingRect.h"

class CSprite;


class CPowerUp
{
public:
	virtual void DoPowerUpLogic() = 0; //TODO: decide on a better pure function maybe Initialise as its this should change for each powerup type?

	virtual bool Deinitialise();
	virtual bool Initialise(int a_x, int a_y);

	virtual void Render();
	virtual void Process(float a_fDeltaTick);
	virtual CBoundingRect GetBoundingRect();

	virtual bool IsFinished();
	virtual void SetFinished(bool status);
	virtual float GetX();
	virtual float GetY();
	virtual int GetPowerUpType();
	virtual void SetPowerUpType(int a_iType);
	
protected:
	float			m_fX;
	float			m_fY;
	float			m_fMoveSpeed;
	int				m_iPowerUpType;
	CSprite			*m_pPowerUpSprite;
	CBoundingRect	m_BoundingRect;
	bool			m_bFinished;
};

