#include "StdAfx.h"
#include "Controller.h"
#include "Level.h"
#include "PlayerShip.h"
#include "SoundFX.h"
#include "Game.h"

CController::CController() 
{
}

CController::~CController(void)
{
}

void CController::ProcessKeyDownMessage(char a_wParam)
{
	switch (a_wParam)
	{
		case 'W':
		{
			break;
		}
		case 'S':
		{
			break;
		}
		case 'D':
		{
			break;
		}
		case 'A':
		{
			break;
		}
		case VK_UP:
		{
			CGame::GetInstance().GetLevel()->GetPlayerShip()->SetState(CPlayerShip::SHIPSTATE_UP, true);
			break;
		}
		case VK_DOWN:
		{
			CGame::GetInstance().GetLevel()->GetPlayerShip()->SetState(CPlayerShip::SHIPSTATE_DOWN, true);
			break;
		}
		case VK_RIGHT:
		{
			CGame::GetInstance().GetLevel()->GetPlayerShip()->SetState(CPlayerShip::SHIPSTATE_RIGHT, true);
			break;
		}
		case VK_LEFT:
		{
			CGame::GetInstance().GetLevel()->GetPlayerShip()->SetState(CPlayerShip::SHIPSTATE_LEFT, true);
			break;
		}
		case VK_SPACE:
		{
			CGame::GetInstance().GetLevel()->SetPlayerShooting(true);
			break;
		}
	}
}


void CController::ProcessKeyUpMessage(char a_wParam)
{
	switch (a_wParam)
	{
		case 'W':
		{
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case 'S':
		{
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case 'D':
		{
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case 'A':
		{
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case VK_UP:
		{
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);

			CGame::GetInstance().GetLevel()->GetPlayerShip()->SetState(CPlayerShip::SHIPSTATE_UP, false);
			break;
		}
		case VK_DOWN:
		{
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);

			CGame::GetInstance().GetLevel()->GetPlayerShip()->SetState(CPlayerShip::SHIPSTATE_DOWN, false);
			break;
		}
		case VK_RIGHT:
		{
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);

			CGame::GetInstance().GetLevel()->GetPlayerShip()->SetState(CPlayerShip::SHIPSTATE_RIGHT, false);
			break;
		}
		case VK_LEFT:
		{
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);

			CGame::GetInstance().GetLevel()->GetPlayerShip()->SetState(CPlayerShip::SHIPSTATE_LEFT, false);
			break;
		}
		case VK_SPACE:
		{
			CGame::GetInstance().GetLevel()->SetPlayerShooting(false);
			//CGame::GetInstance().GetLevel()->CreateProjectile();
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
	}
}