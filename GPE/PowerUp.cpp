#include "StdAfx.h"
#include "PowerUp.h"
#include "Sprite.h"
#include "resource.h"



bool CPowerUp::Deinitialise()
{
	return (true);
}


bool CPowerUp::Initialise(int a_x, int a_y)
{
	m_bFinished = false;
	m_fMoveSpeed = 200.0f;
	m_fX = a_x;
	m_fY = a_y;
	m_iPowerUpType = 0;

	int iMask = IDB_PUHEALTHMASK;
	int iSprite = IDB_PUHEALTHSPRITE;

	m_pPowerUpSprite = new CSprite();
	m_pPowerUpSprite->Initialise(iSprite, iMask);
	m_pPowerUpSprite->SetX(m_fX);
	m_pPowerUpSprite->SetY(m_fY);

	return (true);
}


void CPowerUp::Render()
{
	m_pPowerUpSprite->Render();
}


void CPowerUp::Process(float a_fDeltaTick)
{
	m_fY += m_fMoveSpeed * a_fDeltaTick;

	m_pPowerUpSprite->SetX(m_fX);
	m_pPowerUpSprite->SetY(m_fY);

	//DoPowerUpLogic();
}


bool CPowerUp::IsFinished()
{
	return m_bFinished;
}


void CPowerUp::SetFinished(bool status)
{
	m_bFinished = status;
}


CBoundingRect CPowerUp::GetBoundingRect()
{
	m_BoundingRect.x1 = m_fX - 40;
	m_BoundingRect.x2 = m_fX + 40;

	m_BoundingRect.y1 = m_fY - 40;
	m_BoundingRect.y2 = m_fY + 40;

	return m_BoundingRect;
}


float CPowerUp::GetX()
{
	return m_fX;
}


float CPowerUp::GetY()
{
	return m_fY;
}


int CPowerUp::GetPowerUpType()
{
	return m_iPowerUpType;
}


void CPowerUp::SetPowerUpType(int a_iType)
{
	m_iPowerUpType = a_iType;
}