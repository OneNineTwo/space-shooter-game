#pragma once

#include "BoundingRect.h"

class CBullet
{
public:
	CBullet(void);
	virtual ~CBullet(void);

	virtual bool Deinitialise();
	virtual bool Initialise(float initXpos, float initYpos);

	virtual void Render();
	virtual void Process(float a_fDeltaTick);

	CBoundingRect GetBoundingRectForNextFrame(float a_fDeltaTick); 
	CBoundingRect GetBoundingRect();

	void ExplodeBullet();
	bool GetFinished();
	void SetFinished(bool status);

	float GetX();
	float GetY();

private:
	CAnimatedSprite		*m_pAnim;
	float				m_fX;
	float				m_fY;
	CBoundingRect		m_BoundingRect;

	bool				m_bFinished;
};

