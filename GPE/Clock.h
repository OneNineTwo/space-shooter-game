#pragma once

class CClock
{
	public:
		CClock();
		virtual ~CClock();
		virtual bool Initialise();

		virtual void Process();
		float GetDeltaTick() const;
		float GetTimeElapsed();

	private:
		CClock(const CClock& a_rClock);
		CClock& operator= (const CClock& a_rClock);

		float m_fTimeElapsed;
		float m_fDeltaTime;
		float m_fLastTime;
		float m_fCurrentTime;
};