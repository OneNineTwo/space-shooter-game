#pragma once

class CSprite;

class CPlayerHealth
{
public:
	CPlayerHealth(void);
	virtual ~CPlayerHealth(void);

	virtual bool Deinitialise();
	virtual bool Initialise();

	virtual void Render();
	virtual void Process(float a_fDeltaTick);

	int GetCurrentHealth();
	void SetCurrentHealth(int a_iHealth);
	void SubtractHealth(int a_iNum);
	void IncreaseHealth(int a_iNum);
	

private:
	float				m_fX;
	float				m_fY;
	CSprite				*m_pHealthBarSprite;
	int					m_iCurrentHealth;
};

