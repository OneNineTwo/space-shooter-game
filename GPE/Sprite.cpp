#include "stdafx.h"
#include "resource.h"
#include "Game.h"
#include "BackBuffer.h"
#include "Sprite.h"

CSprite::CSprite() :
	m_iX(0),
	m_iY(0)
{
}

CSprite::~CSprite()
{
	Deinitialise();
}

bool CSprite::Deinitialise()
{
	DeleteObject(m_hSprite);
	DeleteObject(m_hMask);
	return (true);
}

bool CSprite::Initialise(int a_iSpriteResourceID, int a_iMaskResourceID)
{
	m_iCustomWidth = 0;
	m_iCustomHeight = 0;
	m_iSpriteX = 0;
	m_iSpriteX = 0;

	HINSTANCE hInstance = CGame::GetInstance().GetAppInstance();

	m_hSprite = LoadBitmap(hInstance, MAKEINTRESOURCE(a_iSpriteResourceID));
	m_hMask = LoadBitmap(hInstance, MAKEINTRESOURCE(a_iMaskResourceID));

	GetObject(m_hSprite, sizeof(BITMAP), &m_BitmapSprite);
	GetObject(m_hMask, sizeof(BITMAP), &m_BitmapMask);
	return (true);
}

void CSprite::Render()
{
	int iW = GetWidth();
	int iH = GetHeight();
	int iX = m_iX - (iW / 2);
	int iY = m_iY - (iH / 2);
	int iSpriteX = GetSpriteX();
	int iSpriteY = GetSpriteY();

	HDC hSpriteDC = CGame::GetInstance().GetSpriteDC();
	HGDIOBJ hOldObj = SelectObject(hSpriteDC, m_hMask);
	BitBlt(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY, iW, iH, hSpriteDC, iSpriteX, iSpriteY, SRCAND);
	SelectObject(hSpriteDC, m_hSprite);
	BitBlt(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY, iW, iH, hSpriteDC, iSpriteX, iSpriteY, SRCPAINT);
	SelectObject(hSpriteDC, hOldObj);
}

void CSprite::Process(float a_fDeltaTick)
{
	
}

int CSprite::GetWidth() const
{
	if(m_iCustomWidth != 0)
	{
		return m_iCustomWidth;
	}
	else
	{
		return (m_BitmapSprite.bmWidth);
	}
}

int CSprite::GetHeight() const
{
	if(m_iCustomHeight != 0)
	{
		return m_iCustomHeight;
	}
	else
	{
		return (m_BitmapSprite.bmHeight);
	}
}

void CSprite::SetX(int a_iX)
{
	m_iX = a_iX;
}

void CSprite::SetY(int a_iY)
{
	m_iY = a_iY;
}


void CSprite::SetCustomWidth(int a_iX)//set a width for the sprite to display rather then the whole bmp
{
	m_iCustomWidth = a_iX;
}

void CSprite::SetCustomHeight(int a_iY)
{
	m_iCustomHeight = a_iY;
}

void CSprite::SetSpriteX(int a_iX)//set x position in the bmp to display
{
	m_iSpriteX = a_iX;
}

void CSprite::SetSpriteY(int a_iY)
{
	m_iSpriteY = a_iY;
}

int CSprite::GetSpriteX()
{
	if(m_iSpriteX > 0)
	{
		return m_iSpriteX;
	}
	else
	{
		return 0;
	}
}

int CSprite::GetSpriteY()
{
	if(m_iSpriteY > 0)
	{
		return m_iSpriteY;
	}
	else
	{
		return 0;
	}
}