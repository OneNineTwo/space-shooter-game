#pragma once

#include <math.h>
#include <windef.h>

class CBoundingRect
{
public:
	CBoundingRect(void);
	~CBoundingRect(void);

	bool Collides(CBoundingRect& a)
	{
		return (	x1 <= a.x1 + (a.x2 - a.x1) && 
					a.x1 <= x1 + (x2 - x1) && 
					y1 <= a.y1 + (a.y2 - a.y1) && 
					a.y1 <= y1 + (y2 - y1) );
	}

	bool Collides (RECT& a)
	{
		CBoundingRect temp; 
		temp.x1 = static_cast<float>(a.left); 
		temp.x2 = static_cast<float>(a.right); 
		temp.y1 = static_cast<float>(a.top); 
		temp.y2 = static_cast<float>(a.bottom); 

		if(Collides(temp))
		{
			return true; 
		}

		return false; 
	}

	bool Contains(CBoundingRect& a)
	{
		float xPos = a.x1;
		float yPos = a.y1;

		bool inX = x1 < xPos && x2 > xPos;
		bool inY = y1 < yPos && y2 > yPos;

		return (inX && inY);
	}


	bool Contains2(CBoundingRect& otherRect)
	{
		for (int ix=x1; ix<x2; ix++) //for each pixle in this rect
		{
			for (int iy=y1; iy<y2; iy++)
			{
				if( //check each pixle loop if is within the area of other rect
					(ix > otherRect.x1 && ix < otherRect.x2) &&
					(iy > otherRect.y1 && iy < otherRect.y2)
					)
				{
					return true;
				}
			}
		}
		return false;
	}

	float x1; 
	float x2;
	float y1;
	float y2;

};

