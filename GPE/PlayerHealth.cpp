#include "StdAfx.h"
#include "PlayerHealth.h"

#include "Sprite.h"
#include "resource.h"


CPlayerHealth::CPlayerHealth()
{
	m_fX = 700;
	m_fY = 600;
	m_iCurrentHealth = 4;
}


CPlayerHealth::~CPlayerHealth()
{
	Deinitialise();
}


bool CPlayerHealth::Deinitialise()
{
	return (true);
}

bool CPlayerHealth::Initialise()
{
	int iMask = IDB_HEALTHBARMASK;
	int iSprite = IDB_HEALTHBARSPRITE;

	m_pHealthBarSprite = new CSprite();
	m_pHealthBarSprite->Initialise(iSprite, iMask);
	m_pHealthBarSprite->SetX(m_fX);
	m_pHealthBarSprite->SetY(m_fY);
	m_pHealthBarSprite->SetCustomWidth(50);
	m_pHealthBarSprite->SetSpriteX(0);

	return (true);
}

void CPlayerHealth::Render()
{
	m_pHealthBarSprite->Render();
}

void CPlayerHealth::Process(float a_fDeltaTick)
{
	//switch x positions on the sprite depending on how much health the player has
	switch(m_iCurrentHealth)
	{
		case 4:
			m_pHealthBarSprite->SetSpriteX(0);
			break;
		case 3:
			m_pHealthBarSprite->SetSpriteX(50);
			break;
		case 2:
			m_pHealthBarSprite->SetSpriteX(100);
			break;
		case 1:
			m_pHealthBarSprite->SetSpriteX(150);
			break;
		case 0:
			m_pHealthBarSprite->SetSpriteX(200);
			break;
		default:
			m_pHealthBarSprite->SetSpriteX(250);
	}

	m_pHealthBarSprite->Process(a_fDeltaTick);
}

void CPlayerHealth::SetCurrentHealth(int a_iHealth)
{
	m_iCurrentHealth = a_iHealth;
}

int CPlayerHealth::GetCurrentHealth()
{
	return m_iCurrentHealth;
}

void CPlayerHealth::SubtractHealth(int a_iNum)
{
	m_iCurrentHealth -= a_iNum;

	if(m_iCurrentHealth < 0)
	{
		m_iCurrentHealth = 0;
	}
	
}

void CPlayerHealth::IncreaseHealth(int a_iNum)
{
	m_iCurrentHealth += a_iNum;

	if(m_iCurrentHealth > 4)
	{
		m_iCurrentHealth = 4;
	}
	
}