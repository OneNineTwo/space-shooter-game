#include "StdAfx.h"
#include "resource.h"
#include "AnimatedSprite.h"
#include "AlienProjectile1.h"


const float CONST_MOVE_SPEED = 350.0f;

CAlienProjectile1::CAlienProjectile1(void)
{
	//todo: extend projectile class
	//level class to preload all projectiles
	//rename this damn class or move the sprite to a missle class
	//create missle class to extend projectile
	m_fX = 0;
	m_fY = 0;

	m_bFinished = false;
}


CAlienProjectile1::~CAlienProjectile1(void)
{
	Deinitialise();
}


bool CAlienProjectile1::Deinitialise()
{
	return (true);
}

bool CAlienProjectile1::Initialise(float initXpos, float initYpos)
{
	int iMask = IDB_ALIENPROJ1MASK;
	int iSprite = IDB_ALIENPROJ1SPRITE;

	m_fX = initXpos;
	m_fY = initYpos;

	m_pAnim = new CAnimatedSprite();
	m_pAnim->Initialise(iSprite, iMask);
	m_pAnim->SetWidth(12);
	m_pAnim->SetSpeed(0.1f);
	m_pAnim->AddFrame(0);
	m_pAnim->AddFrame(12);

	return (true);
}

void CAlienProjectile1::Render()
{
	m_pAnim->Render();
}

void CAlienProjectile1::Process(float a_fDeltaTick)
{
	m_pAnim->Process(a_fDeltaTick);

	m_fY += CONST_MOVE_SPEED * a_fDeltaTick;

	m_pAnim->SetX(static_cast<int>(m_fX));
	m_pAnim->SetY(static_cast<int>(m_fY));

	if (m_fY > 800) //if bullet goes offscreen delete it (not quite offscreen in this case)
	{
		m_bFinished = true;
	}
}

bool CAlienProjectile1::GetFinished()
{
	return m_bFinished;
}

void CAlienProjectile1::SetFinished(bool status)
{
	m_bFinished = status;
}

void CAlienProjectile1::ExplodeBullet()
{
	//do explode animation?
	//handle here? or after collision in a differnt sprite class

	//TODO: set it as a state, then on collision set the state to explode. play animation then delete it
}


CBoundingRect CAlienProjectile1::GetBoundingRect()
{
	m_BoundingRect.x1 = m_fX - 5;
	m_BoundingRect.x2 = m_fX + 5;

	m_BoundingRect.y1 = m_fY - 24;
	m_BoundingRect.y2 = m_fY + 24;

	return m_BoundingRect;
}


float CAlienProjectile1::GetX()
{
	return m_fX;
}


float CAlienProjectile1::GetY()
{
	return m_fY;
}